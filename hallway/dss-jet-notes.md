---
title: "dss jet notes"
time: 202305261021
type: note
domain: fluids
---

{{< P >}}
  purpose of the paper: review progress in using [DSS] to understand the 
  formation and statistics of [jets].

  - explain the method (DSS) and its relation to other statistical methods
  - describe the strengths/weaknesses of DSS
  - and the strengths/weaknesses of different approximations
  - go over some generalisations of DSS
  - discuss the conservation laws that appear for higher order approximations
  - compare all these methods against the fiducial problem (the stochastically 
    forced jet on a spherical surface).
  - explore the validity of the various QL approximations for the jet problems
  - show insight into mechanisms that may control jet spacing and strength.

{{< P >}}
  real geophysical and astrophysical flows are very far away from idealised 
  [homogeneous isotropic turbulence].

{{< P >}}
  jets form under conditions of: anisotropy and inhomogeneity. 
  - anisotropy: they align perpendicular to the direction of variation in 
    rotation.
  - inhomogeneity: jets are a strong function of position.

# INTRODUCTION TO DSS
--------------------------------------------------------------------------------

focusing on the stochastically driven barotropic jet.

start by describing the method in general terms. then specialise to the case of 
jet formation on a spherical surface.

goal: derive systems that can describe the statistical properties of nonlinear 
systems.

theories of nonequilibrium statistical mechanics can be grouped into:
- those that try to describe the correlations at nonequal times
- " at equal times

nonequal time approaches are complicated.
- what is nonequal time? does it mean equal position?

marston and tobias focus on formulations of equal time statistics.

why is it called the reynolds decomposition? separating a vector into mean and 
fluctuating parts.
