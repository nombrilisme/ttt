---
title: "Turbulent dynamics of pipe flow captured in a reduced model: puff 
relaminarization and localized edge states"
author: "A.P. Willis, R.R. Kerswell"
institution: "J. Fluid Mech"
year: 2009
link:
showlink:
---
