---
title: ttt
project: ttt
type: domain
pagetitle: ttt
cascade:
  favicon: "wave.png"
  colour: giraffe
---

<div class="jean">

<span class="JLGB" style="letter-spacing:0.30em;">
TRANSITIONS TO
</span>
<br> 
<span class="JLGB" style="letter-spacing:0.58em">
TURBULENCE
</span>
<br>

</div>
<br>

{{< figure vorticity.png 9.5 >}}

<br>
<br>
<br>

Find out if direct statistical simulation of pipe flow using CE2 can reproduce 
the transition to turbulence and maintain the DP critical scaling law.

<br>
<br>

## [QUICK LINKS](./)
- {{< subdir/notes methodology >}}
- {{< subdir/notes simulation log >}}
{.notelist2}

## [NOTES](./)
- {{< subdir/notes quasilinear approximation >}}
- {{< subdir/notes transition to turbulence >}}
- {{< subdir/notes pseudo spectral method >}}
- {{< subdir/notes proper orthogonal decomposition >}}
{.notelist2}

