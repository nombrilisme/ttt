---
title: "shear force"
time: 202305060423
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    a force that acts on one part of a body in a certain direction, and another 
    part in the opposite direction.

    contrast to the case where forces are aligned with each other (collinear), 
    in which case they're tension or compression forces.

    {{< figure shear.png >}}
---
