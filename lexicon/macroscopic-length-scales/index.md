---
title: "macroscopic length scales"
time: 202305181035
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    length scales on the order of what we (humans) can see and measure easily.
---
