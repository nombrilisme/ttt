---
time: 202305020723
title: "turbulent viscosity"
type: lexicon
domain: fluids

aka:
- eddy viscosity

what:
  0: |
    an "effective" viscosity you get when you ignore small-scale vortices/eddies 
    (in order to model the large-scale motion). this large scale motion is 
    governed by the _turbulent viscosity_ or _eddy viscosity_ (more common), 
    defined
    ```
      ν₍t₎ ≃ ÷13 v₍l₎ l
    ```
    where
    - ˝l˝ is the size of the largest eddies
    - ˝v₍l₎˝ is the magnitude of the velocity fluctuations
---
