---
time: 202305020905
title: "coanda effect"
type: lexicon
domain: fluids

what:
  0: |
    the tendency of a stream of fluid to stay attached to a gently curved 
    surface that's somewhat adjacent to the flow direction.

    in general: if you have a turbulent flow close to a wall, and the wall 
    starts to _gently_ curve away from the flow, the flow will start to build a 
    pressure gradient that will cause the turbulent flow attached to the wall.

    why: {{< ℑ entrainment >}}. in general a turbulent wake will tend to capture 
    more fluid from outside the wake (entrainment). but if there's a solid wall 
    nearby, there will be no (or little) fluid to actually capture.  thus the 
    entrainment "effect" of the fluid stream will cause the pressure of the 
    fluid to decrease near the wall, and the pressure gradient will move the 
    stream towards the wall.

    the coanda effect is generally seen in turbulent flows, rather than laminar 
    flows, because the entrainment effect in the latter is much weaker (though 
    it's still possible to observe the effect in laminar flows).

src:
  - TB p806
  - https://en.wikipedia.org/wiki/Coand%C4%83_effect
  - https://inv.riverside.rocks/watch?v=AvLwqRCbGKY
---
