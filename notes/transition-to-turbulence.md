---
title: "transition to turbulence in pipes"
type: note
domain: ecc
font: concrete
---

{{< P >}}
  In pipes, the flow transitions from laminar to turbulent when you increase the 
  flow speed.

{{< P >}}
  During the transitional regime the flow develops regions of *localised 
  turbulence* called puffs.

{{< P >}}
  Right now there isn't a good _field theory_ that describes the transition to 
  turbulence. The NS equation is good at describing fluid motion at the 
  microscopic level, but not the transition.

{{< P >}}
  In pipes, the transition depends on a dimensionless parameter, the Reynolds 
  number:
  ```
    Re = ⌠1⧸μ⌡ ρuL
  ```
  ```
    Re = ⌠1⧸ν⌡ LV
  ```
  - ˝μ˝: dynamic viscosity (absolute viscosity)
  - ˝ν˝: kinematic viscosity
  - ˝ρ˝: density
  - ˝L˝: characteristic length scale
  - ˝u˝: bulk velocity (cross-sectionally averaged mean flow speed)
  - ˝V˝: characteristic flow velocity

  As you increase Re, the flow eventually transitions from laminar to turbulent.

  {{< figure transition-illustration.png 7 >}}
  {{< caption >}}
  {{< ℭ CZ >}}
  {{< /caption >}}

{{< P >}}
  If the pipe flow is driven by pressure, you need to model how much pressure 
  you need to induce the transition to turbulence.

{{< P >}}
  You can describe the transition to turbulence in terms of a variable called 
  the friction factor:
  ```
    f = ⌠(Δp)d ⧸ ½ρL₀u²⌡
  ```
  - ˝Δp˝: the pressure differential or pressure gradient over the ends of the 
    pipe (this is what drives the flow)
  - ˝d˝: pipe diameter
  - ˝ρ˝: density
  - ˝u˝: bulk velocity
  - ˝L₀˝: stremwise distance between pressure differential (the length across 
    which the pressure gradient exists).

  {{< figure friction-factor-vs-Re.png >}}
  {{< caption >}}
  Green dots: experimental data when a transition is observed. White dots: what 
  you *could* observe if the experiment is very highly controlled to minimise 
  disturbances. the flow stays laminar even as Re increases. {{< ℭ CZ >}}
  {{< /caption >}}

  At low Re, the flow is laminar. the friction factor follows the scaling 
  relationship derived from the NSE. at high Re the flow is turbulent, and the 
  friction factor follows Prandtl's law. 

  At a given Re (in the transitional region) the friction factor in the 
  turbulent state is higher than in the laminar state. This is because friction 
  drag increases in the fluid when turbulent motion is present.

{{< P >}}
  There is evidence that the transition to turbulence is subcritical. Notice in 
  the prev figure the white dots that extend the laminar law into the 
  transitional regime and beyond. If you have a laminar flow in an environment 
  very free of disturbances (the disturbances that trigger turbulence) then the 
  laminar friction law extends to higher Re.

  What this means is that turbulence *requires* a finite amount of disturbance 
  to get triggered. __The transition is subcritical__.

  There are numerical and experimental sims that show you can maintain stable 
  laminar pipe flow to infinitesimal disturbances up to a certain Re.

{{< P >}}
  The transition to turbulence is not really a phase transition, in the sense of 
  statistical mechanics. Because the system is is non-equilibrium.

  Numerical and experimental sims: full turbulence *arises from puffs* which 
  split up and eventually take over the whole domain.

{{< P >}}
  As the system evolves, puffs either die or split up. 

  {{< ℭ WK >}}: studies on the lifetime of puffs. Observed that the mean lifetime of a 
  puff, ˝τ˝, is a double exponential of the Re:
  ```
    ln(ln(τ)) = Re
  ```
  or
  ```
    τ = exp(exp(Re))
  ```

  This suggests the entire phenomenon of turbulence is just a *transient state* 
  of a very long survival time. We don't yet know how exactly this works, and 
  how it relates to the NSE.

{{< P >}}
  Pomeau: the laminar state is an *absorbing state*, and the LT transition is 
  governed by a process in the DP universality class.

  DP is a class of models that mimic the filtering of fluids through porous 
  materials. Analogy: a fluid trying to flow through a series of bonds/barries 
  that have different connectivities.

  The penetration probability: probability that the bond is OPEN to let the 
  fluid pass through. 

  The value of the penetration probability can cause the system to have a phase 
  transition from a *macroscopically permeable state* (fluids can readily pass 
  through the pores) to a *macroscopically impermeable state* (fluids can't pass 
  through the pores). obviously, the impermeable state strongly restricts fluid 
  flow.

  The macroscopically IMPERMEABLE state: analogue of the laminar state.

  When you change the Re, the penetration probability changes, and the system 
  goes from the turbulent state (puff survival) to the laminar state (puff 
  extinction).

{{< P >}}
  {{< ℭ CZ >}}: used the QL approximation as a bridge find a link between the 
  transition to turbulence and statistical mechanics.

{{< P >}}
  La problème: we've observed experimentally that laminar flow is stable to 
  infinitesimal disturbances at Re way above the transitional region (up to a 
  certain Re, that is).

{{< P >}}
  In 1986 pomeau suggested the transition is part of the DP universality class. 
  means the transition is continuous wrt the penetration probability.

  BUT experiments showed a discontinuity in the order parameter in plane couette 
  flows (contradicting DP).

  In 2011 avila found that turbulence can be induced by spatial coupling of 
  chaotic domains. thus you need large enough domain to actually observe the 
  critical behaviour in the transition.

{{< P >}}
  In 2016: shih et al showed a relation between the L-T transition and DP. 
  linked pipe flow system with predator prey system.

  The PP system has a phase transition in the DP universality class at the limit 
  of prey extinction. a zonal flow emerges because of the presence of turbulent 
  flow. 

  The zonal flow is the predator. the turbulent flow is the prey. the laminar 
  flow is the nutrient.
