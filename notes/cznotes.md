---
title: "cznotes"
type: note
domain: ecc
---

{{< P >}}
  Fluids have complex nonlinear interactions on a large range of spatial and 
  temporal scales. The PDEs involved have high order nonlinear terms that are 
  too computationally expensive to resolve precisely. To find a practical 
  solution, you therefore have to do some coarse-graining.

{{< P >}}
  DSS -- direct statistical simulation -- finding a statistical description of 
  the system.

  One example of DSS: solve the cumulant expansion directly, and truncate at 
  second order. This is called CE2.

  We know that CE2 can represent the statistics of barotropic jets well.

  CE2 has exact closure in the QL approximation.

  By default, CE2 can have large dimensionality. So to model turbulence using 
  CE2 you use the quasilinear approximation and reduce the dimension of the 
  problem in the QL step.

{{< P >}}
  QL is a mean field theory. It separates the flow into a mean field and a 
  perturbation field.

  Triadic diagrams in wavenumber space. squiggly lines represents a wave mode 
  with wave number ˝m˝. straight line is wave mode 0, i.e. the mean flow.

  linear approximation: interaction between waves and mean flow only.

  QL approximation: 
  - interaction between the wave and mean flow (LINEAR)
  - interaction between waves of equal and opposite wave number (e.g. m = 3 and 
    m = -3).

  NL:
  - interaction between wave and mean flow (LINEAR)
  - interaction between waves of equal and opposite wavenumber (QL)
  - interaction between waves and waves (wave + wave → wave scattering, i.e.  
    waves of different wave numbers scatter to different wave modes).

{{< P >}}
  Reynolds decomposition: separate the flow field into its mean flow and 
  fluctuation:
  ```
    ⬀u = ⟨⬀u⟩ + δ⬀u
  ```


