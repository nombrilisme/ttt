---
time: 202308281203
title: "rql simnotes"
type: note
domain: fluids
---

# PROJECT GOAL: USING CE2 TO MODEL LTT
--------------------------------------------------------------------------------

{{< P >}}
  HYPOTHESIS: it *is* possible to model LTT using CE2.

  We have reason to believe this hypothesis because of the following conclusions 
  about QL, DSS, and CE2: 

  - QL demonstrably captures the correct critical behavior of the LT transition 
    (in annular pipes anyway)
  - CE2 has exact closure in QL

  ∴ since CE2 has exact closure in QL, and since QL captures the critical 
  behaviour of the LTT, it follows that CE2 could be used model the LTT.


{{< P >}}
  But for all this to work, you need to set up a viable configuration of CE2.
  This is nontrivial because the variable of interest in CE2 (namely, the second 
  cumulant) technically has *higher* dimension than the field itself (that is, 
  the flow field ˝⬀u˝, the variable of interest in DNS). Obviously, higher 
  dimension variables are more expensive to compute. This means that many 
  configurations of CE2 are by default *more* computationally expensive than 
  DNS.

  So for CE2 to be practically viable you have to reduce this extra 
  dimensionality (using POD, for instance).

  The 2nd cumulant, note:
  ```
    ⬀c(⬀r₁,⬀r₂) = ⟨⬀u(⬀r₁) ⊗ ⬀u(⬀r₂)⟩
  ```
  is 1d larger than the velocity field ˝⬀u(⬀r)˝. 


{{< P >}}
  GOAL: implement CE2 for pipe flow and reduce the size of the model AMAP. Find 
  the most reduced model that preserves both the LTT *and* the critical scaling 
  law (both of which are observed in QL).

{{< P >}}
  To reduce the model, use POD (proper orthogonal decomposition), a 
  dimensionality reduction method (next up).

{{< P >}}
  CZ: lost the critical behaviour at severe truncation levels. CZ's 
  implementation of CE2 only averaged over θ. 

{{< P >}}
  WK: captured critical behaviour in the LTT with a very reduced model that had 
  only *one* nonzero wave mode in the θ direction. But they didn't investigate 
  the critical scaling law.


# POD
--------------------------------------------------------------------------------

{{< P >}}
  alg: average over θ. 

  mean coordinate:
  ```
    ⟨θ⟩ = ½(θ₁ + θ₂)
  ```

  deviation:
  ```
    δθ = θ₁ - θ₂
  ```

  compute the correlation. F transform (np.fft) over θ.

  to get the equation, take the mth slice of each scalar field matrix, do CZ 
  e4.4, and you'll get for each δθ the column of the covmatrix that varies over 
  δθ.

  ignore sum over m. look@ contributions to covmatrix at each wvnum m 
  separately. will get an array of dim 4N₍r₎N₍z₎ × 4N₍r₎N₍z₎ at each time t for 
  each zonal wavenumber m. this matrix will have one nonzero eigenvalue for each 
  zonal wavenumber m.

  so ˝c⟦ijm⟧(r₁,z₁,r₂,z₂,t)˝ is a rank 1 matrix.

{{< P >}}
  covariance matrix:
  ```
    c⟦ijm⟧(r₁,z₁,r₂,z₂,t) = q⟦im⟧(r₁,z₁,t) q⟦jm⟧(r₂,z₂,t)⁽*⁾
  ```
  ```
    = c⟦jim⟧(r₂,z₂,r₁,z₁,t)⁽*⁾
  ```
  this matrix should be real and nonnegative. use to verify CE2. 
  - sitrep: THEY ARE!

{{< P >}}
  to average over z as well, F transform to wavevector k₍z₎ space. for each 
  value of m *and* k₍z₎ there will be one nonzero eigenvalue.

{{< P >}}
  the eigenbasis of the covmatrix averaging over θ is the F basis over θ. the 
  eigvals are the corresponding F coefficients. don't have to deal with the 
  matrix diagonalisation algs. just check the power spectra.

{{< P >}}
  reducing the model:

  1. the PCs in QL is the F basis in the averaging (θ) direction. so just plot 
  the cumulative explaine dvariance (the cumulative power spectrum over the θ 
  direction).

  2. select the reduced model based on how much variance to keep.

  3. plot the c. pwr spectra over the other two directions, r and z, to decide 
  how much resolution to keep in the reduced model.

  4. simualte over a range of Re numbers and look for critical. if you find it, 
  go down to a more reduced model until you find the smallest possible on

  4. simualte over a range of Re numbers and look for critical. if you find it, 
  go down to a more reduced model until you find the smallest possible one.

# SNOTE
--------------------------------------------------------------------------------

{{< P >}}
  how observing the transition: measuring the turbulent fraction. look at puff 
  formation and splitting by plotting the streamwise vorticity.

{{< P >}}
  the turbulent fraction at the critical point and higher Re numbers are very 
  close to each other.

{{< P >}}
  to see critical DP scaling: increase number of modes to puff structure is 
  better resolved. or expand domain to be a bigger pipe.

# SIMULATIONS RUN
--------------------------------------------------------------------------------

full QL: 100, 128, 512. see localised puffs, transition to turbulence, around 
Re=3000.

CZ's fully reduced model: 50, 4 128. can see transition, puff elongation, but no 
DP scaling.
  - NL 100 256 512. CRe 1800
  - QL 100 128 512. CRe 2800
  - QL 100 8 128. CRe 5590
  - QL 50 4 128. CRe 13000

problem: turbulent fraction at critical point and higher Re numbers are very 
similar.

my minimal model that reproduces DP? started at QL 100 128 512, reduced the 
number of θ modes about 34. Re 3900. smallest model I could get that resembled 
DP scaling within a standard deviation. beyond that the points start flattening.
100, 34, 512. 

averaging over z as well: harder to identify what the critical Re is. harder to 
observe localised puffs and puff splitting.

increasing the pipe length: 5x larger than CZ, the puff is localised, but no 
critical DP scaling.
