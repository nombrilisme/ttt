---
title: "the quasilinear approximation"
type: note
domain: fluids
font: concrete
---

# FLOW INTERACTIONS RETAINED IN QL
--------------------------------------------------------------------------------

{{< P >}}
  There are three kinds of interactions in the flow. 1) Eddies (waves) interact 
  with the mean flow. 2) The eddies themselves produce a Reynolds stress that 
  drives the mean flow. 3) There's a nonlinear interaction when two waves 
  combine to produce a third wave of different wavenumber. {{< ℭ MT >}}

  These three interactions represented here by these triad diagrams (case of 
  spatial averaging where the mean corresponds to the zonal wavenumber ˝k=0˝):
  
  {{< figure ql-triad-diagrams.png >}}
  {{< caption >}}
  Triads in wavevector space. Squiggly line: a wave mode with wave number ˝m˝.  
  Straight line: the mean flow, i.e. wave mode ˝m=0˝.  {{< ℭ MT >}} {{< ℭ CZ >}}
  {{< /caption >}}
  
  Left: the __wave↔mean interaction__, i.e. the interaction between waves (of 
  wavenumber ˝m˝) and the mean flow (wavenumber ˝m=0˝).

  Middle: __Reynolds forcing of the mean flow__, i.e. the interaction between 
  waves of equal and opposite wavenumber, e.g. wavenumbers ˝m=3˝ and ˝m=-3˝. 
  These interactions are nonlinear and they contribute to the mean flow (hence 
  the name).

  Right: __wave↔wave interaction__, i.e. the nonlinear interaction between waves 
  of different wavenumbers. Leads to wave scatterings of higher wavenumbers.  
  Also called cascades or inverse cascades. The QL approximation discards this 
  interaction. {{< ℭ MT >}} {{< ℭ CZ >}}

  Linear approximation: keeps only the interaction between the waves and the 
  mean flow (the wave↔mean-flow interaction, i.e. the left triad). 

  QL approximation: keeps both the wave↔mean-flow interaction _and_ 
  interactions between waves of equal and opposite wavenumbers.

  Fully nonlinear: keeps all three kinds of interactions.

{{< P >}}
  The QL approximation is a closed system: energy and angular momentum is 
  conserved. This is one of the benefits of dropping the 
  eddy+eddy→eddy-scattering processes. {{< ℭ MT >}} {{< ℭ CZ >}}

{{< P >}}
  The QL approximation is best in conditions where the mean flow is strong and 
  the eddies are weak. This is because discarding the eddy+eddy→eddy interaction 
  means energy is concentrated in the lower wavenumbers, particularly in waves 
  that are marginally unstable in the mean flow. {{< ℭ MT >}}

{{< P >}}
  Three limiting cases where the QL approximation is asymptotically exact:

  1. case where waves are strongly damped due to large friction or dissipation.
  2. case where there's a large time-scale separation between slowly evolving 
  mean flow and rapidly evolving eddies.
  3. case where you stimulate only a single wavenumber (with stochastic forcing 
  that's spectrally sharp in wavenumber space). In this case no other waves will 
  become excited if you keep the mean flow strength under the threshold of 
  instability. {{< ℭ MT >}}


# EQUATIONS OF MOTION AND AVERAGING
--------------------------------------------------------------------------------

{{< P >}}
  QL is a mean field theory. It involves separating the field into two 
  components: a mean field and a perturbation field. {{< ℭ MT >}} {{< ℭ CZ >}} 

  Here the field in question is the fluid flow field, ˝⬀u˝.

{{< P >}}
  The EOMs we deal with have quadratic nonlinearities, due to advection. {{< ℭ 
  MT >}}. In these sorts of systems you can express the PDE as the sum of a 
  linear term and a nonlinear term:
  ```
    𝝏₍t₎⬀u = L[⬀u] + N[⬀u,⬀u]
  ```
  where ˝L[ͺ]˝ is a linear operator. ˝N[ͺ,ͺ]˝ is a nonlinear quadratic operator.
  {{< ℭ MT >}} {{< ℭ CZ >}}


{{< P >}}
  Reynolds decomposition: express the flow field as the sum of a mean flow and a 
  fluctuation:
  ```
    ⬀u = ⟨⬀u⟩ + δ⬀u
  ```
  
  The averaging operator ˝⟨⋄⟩˝ satisfies
  ```m
    ⟨⟨⬀u⟩⟩ = ⟨⬀u⟩
    ⟨δ⬀u⟩  = 0
    ⟨⟨⬀u⟩ ⬀u⟩ = ⟨⬀u ⬀u⟩
  ```

{{< P >}}
  There are many ways to actually do the averaging, e.g. you could do spatial, 
  temporal, ensemble. {{< ℭ MT >}} Quantities in angled brackets denote averaged 
  quantities (after you've chosen an averaging method).

  Spatial averaging is the most common method. You should choose the spatial 
  averaging direction based on the symmetries of the system, e.g. in large scale 
  flows you should average over the the zonal direction. {{< ℭ MT >}}


{{< P >}}
  After you do the Reynold's decomposition and do the averaging, the EOM form 
  for the mean field is:
  ```
    𝝏₍t₎(⟨⬀u⟩) = L[⟨⬀u⟩] + N[⟨⬀u⟩,⟨⬀u⟩] + ⟨N[δ⬀u,δ⬀u]⟩
  ```

  and for the fluctuating field:
  ```mm
    𝝏₍t₎(δ⬀u) &= ( L[δ⬀u] + N[⟨⬀u⟩,δ⬀u] + N[δ⬀u,⟨⬀u⟩] )
    &+ ( N[δ⬀u,δ⬀u] - ⟨N[δ⬀u,δ⬀u]⟩ )
  ```
  The term in the top line is the quasilinear term for the fluctuating part of 
  the flow field. The term in the bottom line represents the eddy+eddy→eddy 
  scattering interactions. {{< ℭ CZ >}} {{< ℭ MT >}}

  In QL you omit the last term. Obviously, this approximation is only valid when 
  this scattering term is small compared to the quasilinear term. This condition 
  arises when the Kubo number is small, which can happen if when low amplitude 
  wave turbulence interacts with mean flows (quasilinear turbulent 
  interactions). {{< ℭ MT p6-7 >}} 


{{< P >}}
  So the QL EOMS are:
  ```
    𝝏₍t₎(⟨⬀u⟩) = L(⟨⬀u⟩) + N(⟨⬀u⟩,⟨⬀u⟩) + ⟨N(δ⬀u,δ⬀u)⟩
  ```
  ```
    𝝏₍t₎(δ⬀u) = L(δ⬀u) + N(⟨⬀u⟩,δ⬀u) + N(δ⬀u,⟨⬀u⟩)
  ```

