---
title: "dss"
type: note
domain: ecc
---

{{< P >}}
  DSS (direct statistical simulation) means finding solutions to 
  equations that describe the *statistical* properties of a system {{< ℭ MTIAT 
  >}}. you have to statistically *truncate* the system {{< ℭ CZ ch1.1 >}}.

{{< P >}}
  CE2 is an example of DSS. you do a direct cumulant expansion and truncate at 
  the second order {{< ℭ CZ ch1.1 >}}.

{{< P >}}
  numerical simulation vs DSS: little diagram showing the hierarchy of 
  approximation schemes for numerical simulation and the equivalent DSS method:

  {{< figure numerical-vs-dss-evolution.jpg >}}
  {{< caption >}}
  {{< ℭ MT >}}
  {{< /caption >}}


{{< P >}}
  benefits of CE2: has exact closure in the QL approximation.

{{< P >}}
  QL is a mean field theory: it decomposes the equation into a mean field and a 
  perturbation field {{< ℭ CZ >}}.

{{< P >}}
  triad diagrams in wavenumber space. ˝m˝ is the wavenumber. the straight line 
  with ˝m=0˝ is the mean flow.

  {{< figure ql-triad-diagrams.png >}}
  - left: wave-mean flow interaction
  - middle: reynolds forcing of mean flow
  - right: wave-wave interaction. omitted in the QL approximation.

  {{< ℭ MTIAT >}} {{< ℭ CZ >}}

{{< P >}}
  two consequences of omitting the wave-wave interactions: 

  1. there are no scatterings to higher wavenumbers. either the scatterings 
  between waves stay within their own wave modes OR they get reduced to the mean 
  flow.

  2. the QL system is closed, so energy is conserved.

