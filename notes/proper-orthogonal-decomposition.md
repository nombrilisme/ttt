---
title: "proper orthogonal decomposition"
type: note
domain: fluids
---

{{< P >}}
  POD is a dimensionality reduction method. aka PCA (principal component 
  analysis). to reduce the dimensionality of the basis. based on a change of 
  basis.

  - diagonalise the covariance matrix to get the eigenbasis (of the cov matrix)
  - these eigenvectors are the principal components
  - the eigenvalues of the eigenvectors represent how much variance the 
    corresponding eigenvector explains.
  
  to reduce the basis by, you select the most important eigenvectors.

  you can recover the original basis via a linear transformation.

{{< P >}}
  let ˝⬀u˝ be the fluid's velocity field, ˝⬀u = [u₍r₎, u₍θ₎, u₍z₎]˝.

  let ˝p˝ be the fluid's pressure field (defined at every point in the fluid).
  
  let ˝q˝ be a variable representing the four fields in the fluid, so ˝q = [u₍r₎, 
  u₍z₎, u₍θ₎, p]˝, and ˝q₍1₎˝ is the velocity field in the ˝r˝ direction, ˝q₍4₎˝ 
  is the pressure field, etc.

  let ˝N₍r₎, N₍θ₎, N₍z₎˝ be the number of modes in the ˝r,θ,z˝ directions, 
  respectively.

  so ˝q(r,θ,z,t)˝ is a 4D array. 

  average over the azimuthal angle θ.  

  define the mean coordinate:
  ```
    ⟨θ⟩ = ½(θ₁ + θ₂)
  ```
  and the deviation:
  ```
    δθ = θ₁ = θ₂
  ```

  remember, the 1st cumulant:
  ```
    ⬀c(⬀r) = ⟨⬀u(⬀r)⟩
  ```

  the 2nd cumulant (2pt correlation function):
  ```
    ⬀c(⬀r₁,⬀r₂) = ⟨⬀u'(r₁) ⊗ ⬀u'(r₂)⟩
  ```

  so
  ```mm
    c⟦ij⟧(r₁,z₁,r₂,z₂,δθ) 
    &= ⟨q⟦i⟧(r₁,θ₁,z₁,t) q⟦j⟧(r₂,θ₂,z₂,t)⟩
    &= ⌠1⧸2π⌡ ∫{0}{2π} d⟨θ⟩ ͺ q⟦i⟧(r₁,z₁,⟨θ⟩+½δθ,t) q⟦j⟧(r₂,z₂,⟨θ⟩-½δθ,t)
  ```

  now, fourier transform over the θ direction:
  ```
    q⟦i⟧(r₁,θ₁,z₁,t) = ∑{m₁ = -½N₍θ₎+1 ... ½N₍θ₎-1} q⟦i⟧₍m₁₎(r₁,z₁,t) e⁽i m₁ θ₁⁾
  ```
  ```
    q⟦j⟧(r₂,θ₂,z₂,t) = ∑{m₂ = -½N₍θ₎+1 ... ½N₍θ₎-1} q⟦j⟧₍m₁₎(r₂,z₂,t) e⁽i m₂ θ₂⁾
  ```

  using the fourier transform, the 2 point correlation function becomes:
  ```m
    c⟦ij⟧(r₁,z₁,r₂,z₂,θ₁-θ₂,t) 
    = ⌠1⧸2π⌡ ∫ d⟨θ⟩ ∑{m₁,m₂ = -½N₍θ₎+1 ... ½N₍θ₎-1} q⟦i⟧₍m₁₎(r₁,z₁,t) q⟦j⟧₍m₂₎(r₂,z₂,t) 
      ͺͺ ⋄ ͺexp(i m₁ (⟨θ⟩+½δθ)) exp(i m₂ (⟨θ⟩-½δθ))
  ```
  and
  ```
    = ∑{m₁,m₂ = -½N₍θ₎+1 ... ½N₍θ₎-1} 
      q⟦i⟧₍m₁₎(r₁,z₁,t) q⟦j⟧₍m₂₎(r₂,z₂,t)
      δ₍m₁,-m₂₎ exp(i(m₁-m₂)½δθ)
  ```
  ```
    = ∑{m₁,m₂ = -½N₍θ₎+1 ... ½N₍θ₎-1} 
      q⟦i⟧₍m₁₎(r₁,z₁,t) q⟦j⟧₍m₂₎(r₂,z₂,t)⁽*⁾
      e⁽imδθ⁾ 
  ```

  then take the mth slice of each scalar field matrix, ˝q⟦i⟧(m)˝, and do
  ```
    ⌠1⧸N₍θ₎⌡ ∑{m} q⟦i⟧₍m₎(r₁,z₁,t) q⟦j⟧₍m₎(r₂,z₂,t)⁽*⁾ e⁽imδθ⁾
  ```
  do this calculation for each δθ. this will give you the column of the 
  covariance matrix that varies over δθ.

  ignore the sum over m, and look at the contributions to the covariance matrix 
  at each m wavenumber separately
  ```
    c⟦ijm⟧(r₁,z₁,r₂,z₂,t) = q⟦i⟧₍m₎(r₁,z₁,t) q⟦j⟧₍m₎(r₂,z₂,t)⁽*⁾
  ```
  this is an array of dimension (4N₍r₎N₍z₎)×(4N₍r₎N₍z₎) at each time ˝t˝ and for 
  each zonal wavenumber ˝m˝. this matrix projects onto ˝q⟦i⟧₍m₎(r₁,z₁,t)˝ so you 
  can only have at most one nonzero eigenvalue for each zonal wavenumber ˝m˝. 

  this means that ˝c⟦ijm⟧(r₁,z₁,r₂,z₂,t)˝ is a rank 1 matrix. in bra ket 
  notation, it's 
  ```
    c₍m₎(t) = ⎾q₍m₎(t)⟩⟨q₍m₎(t)⏋
  ```
  and its nonzero eigenvalue is
  ```
    ⟨q₍m₎(t)∤q₍m₎(t)⟩ = 2π ∑{i} ∫{R₁}{R₂} drͺr ∫{0}{L} dzͺ|q⟦im⟧(r,z,t)|²
  ```
  you don't have to actually diagonalise because the eigenvalue is given by this 
  inner product.

  note the covariance matrix is hermitian:
  ```
    c⟦ijm⟧(r₁,z₁,r₂,z₂,t) = c⟦ijm⟧(r₂,z₂,r₁,z₁,t)⁽*⁾
  ```
  and it's also semipositive definite. means all the eigenvalues are real and 
  non-negative. use this to check validity of code.

{{< P >}}
  so far, only average over the azimuthal angle θ. you can get more dimensional 
  reduction by averaging over ˝⟨z⟩˝.

  what you do:

  - fourier transform to wavevector ˝k₍z₎˝ space (use same procedure for the θ 
    variable)
  - then, for *each* value of ˝m˝ and ˝k₍z₎˝ there will be a single nonzero 
    eigenvalue.

  so anyway, the equation
  ```
    c⟦ijm⟧(r₁,z₁,r₂,z₂,t) = q⟦im⟧(r₁,z₁,t) q⟦jm⟧(r₂,z₂,t)⁽*⁾
  ```
  what it says is: in QL, the eigenbasis of the covariance matrix averaging over 
  ˝θ˝ is the fourier basis over ˝θ˝. the eigenvalues are the corresponding 
  fourier coefficients. for this reason, you don't have to do any matrix 
  diagonalisation. all you need to do is check the power spectra.

{{< P >}}
  using POD to find the minimal model that produces the transition:

  1. the principal components in QL is the fourier basis along the averaging
  direction (θ). so plot the cumulative explained variance, i.e. the cumulative 
  power spectrum over the θ direction.

  2. choose the reduced model based on how much variance to keep in the system.

  3. plot the cumulative power spectra over the other two directions, r and z, 
  to decide how much resolution to keep in the reduced model. 

  4. simulate over a range of Re numbers to look for critical behaviour. if you 
  see it, go down to a more reduced model, until you find the minimal model.
