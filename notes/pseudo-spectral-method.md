---
title: "pseudo spectral method"
type: note
domain: fluids
font: concrete
---

{{< P >}}
  Finite element method: divides interval into sub intervals, solves the PDEs 
  locally at neighbouring intervals.

{{< P >}}
  Spectral methods: discretise variables by expand over the entire domain using 
  global basis functions. The solution is the set of coefficients for the basis.

{{< P >}}
  Start: choose a set of basis functions that span the domain. 

  For pipe flow, choose
  - fourier series for the periodic domain (azimuthal direction θ) and the 
    streamwise (axial) direction z.
  - chebyshev series for the nonperiodic (radial) direction r.

{{< P >}}
  Pure spectral method: calculates the nonlinear terms in the fourier space 
  representation. This is expensive.

{{< P >}}
  Pseudo spectral: not purely spectral. Evaluates nonlinear terms in real space, 
  then transfers back to spectral space for next time step.

{{< P >}}
  In PS you divide the domain into sub intervals. Dividing points: COLLOCATION 
  POINTS. You solve the PDE on the collocation points. Not necessarily evenly 
  spaced. Higher density at ends to resolve sharp features around the boundary.

{{< P >}}
  Consider an orthonormal basis set ˝[φ⟦n⟧(x)]˝. 

  A function ˝f(x)˝ can be represented in this basis as:
  ```
    f(x) = ∑{n=0..∞} f⟦n⟧ φ⟦n⟧(x)
  ```

  where ˝f⟦n⟧ = ⟨φ⟦n⟧(x) ∤ f(x)⟩˝ is the inner product of ˝φ⟦n⟧(x)˝ and ˝f(x)˝.

{{< P >}}
  Numerically you approximate the functions with a truncated set of finite basis 
  functions and their values at collocation points:
  ```
    f(x) ≈ ∑{n=0..(N-1)} f⟦n⟧ φ⟦n⟧(x⟦n⟧)  
  ```
  ```
    f⟦n⟧(x) = ∑{n=0..(N-1)} w⟦n⟧ f(x⟦n⟧)
  ```

{{< P >}}
  The spectral coefficients decay exponentially for smooth functions. This makes 
  the results accurate.
