---
title: "goal"
type: note
domain: ecc
---

Implement the direct statistical simulation of pipe flow to find out if a 
second-order cumulant expansion can reproduce the transition to turbulence.
